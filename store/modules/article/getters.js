import Constants from './constants'
import state from './state'

export default {

    [Constants.DO_ARTICLE_LIST_ALL]: (state) => {
        return state.articleList
    },
    [Constants.GET_TOTAL_ITEM_COUNT]: (state) => {
        return state.totalItemCount
    },
    [Constants.GET_TOTAL_PAGE]: (state) => {
        return state.totalPage
    },
    [Constants.GET_CURRENT_PAGE]: (state) => {
        return state.currentPage
    },
}
