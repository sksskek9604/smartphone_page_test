export default {
    DO_CREATE: 'article/doCreate',
    DO_ARTICLE_LIST_ALL: 'article/doArticleListAll',

    FETCH_ARTICLE_LIST_ALL: 'article/fetchArticleListAll',
    FETCH_TOTAL_ITEM_COUNT: 'article/fetchTotalItemCount',
    FETCH_TOTAL_PAGE: 'article/fetchTotalPage',
    FETCH_CURRENT_PAGE: 'article/fetchCurrentPage',

    GET_TOTAL_ITEM_COUNT: 'article/getTotalItemCount',
    GET_TOTAL_PAGE: 'article/getTotalPage',
    GET_CURRENT_PAGE: 'article/getCurrentPage',
}
