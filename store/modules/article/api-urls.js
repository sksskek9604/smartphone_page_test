const BASE_URL = '/v1/article'

export default {
    DO_CREATE: `${BASE_URL}/new`, //post
    DO_ARTICLE_LIST_ALL: `${BASE_URL}/list/all`, //get
}
