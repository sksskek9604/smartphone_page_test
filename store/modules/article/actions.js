import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    /**
     * 게시물 등록
     * @param store
     * @param payload
     * @returns {Promise<AxiosResponse<any>>}
     */
    [Constants.DO_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_CREATE, payload)
    },

    /**
     * 전체 게시물 (내용 포함)
     * @param store
     * @returns {Promise<AxiosResponse<any>>}
     */
    [Constants.DO_ARTICLE_LIST_ALL]: (store) => {
        return axios.get(apiUrls.DO_ARTICLE_LIST_ALL)
    },
}
