import Constants from './constants'

export default {

    [Constants.FETCH_ARTICLE_LIST_ALL]: (state, payload) => {
        state.articleList = payload
    },
    [Constants.FETCH_TOTAL_ITEM_COUNT]: (state, payload) => {
        state.totalItemCount = payload
    },
    [Constants.FETCH_TOTAL_PAGE]: (state, payload) => {
        state.totalPage = payload
    },
    [Constants.FETCH_CURRENT_PAGE]: (state, payload) => {
        state.currentPage = payload
    },
}
