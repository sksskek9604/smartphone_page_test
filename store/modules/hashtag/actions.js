import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_HASHTAG_LIST]: (store, payload) => {
        return axios.get(apiUrls.DO_HASHTAG_LIST.replace('{articleId}', payload.id))
    },

    [Constants.DO_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_CREATE, payload)
    },
}
