const state = () => ({

    hashtagList: [],
    currentPage: 1,
    totalItemCount: 0,
    totalPage: 0
})

export default state
