const BASE_URL = '/v1/hash-tag'

export default {
    DO_HASHTAG_LIST: `${BASE_URL}/list/{articleId}`, //get
    DO_CREATE: `${BASE_URL}/new`, //post

}
