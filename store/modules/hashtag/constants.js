export default {
    DO_HASHTAG_LIST: 'hashtag/doHashtagList',
    DO_CREATE: 'hashtag/doCreate',

    FETCH_TOTAL_ITEM_COUNT: 'hashtag/fetchTotalItemCount',
    FETCH_TOTAL_PAGE: 'hashtag/fetchTotalPage',
    FETCH_CURRENT_PAGE: 'hashtag/fetchCurrentPage',

    GET_TOTAL_ITEM_COUNT: 'hashtag/getTotalItemCount',
    GET_TOTAL_PAGE: 'hashtag/getTotalPage',
    GET_CURRENT_PAGE: 'hashtag/getCurrentPage',

}
