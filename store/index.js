import customLoading from './modules/custom-loading'
import menu from './modules/menu'
import article from './modules/article'
import hashtag from './modules/hashtag'


export const state = () => ({})

export const mutations = {}

export const modules = {
    customLoading,
    menu,
    article,
    hashtag
}
