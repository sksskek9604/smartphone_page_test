import Vue from 'vue'

import customLoadingConstants from '~/store/modules/custom-loading/constants'
Vue.prototype.$customLoadingConstants = customLoadingConstants

import menuConstants from '~/store/modules/menu/constants'
Vue.prototype.$menuConstants = menuConstants

import articleConstants from '~/store/modules/article/constants'
Vue.prototype.$articleConstants = articleConstants

import hashtagConstants from '~/store/modules/hashtag/constants'
Vue.prototype.$hashtagConstants = hashtagConstants
